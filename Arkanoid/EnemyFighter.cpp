//
//  EnemyFighter.cpp
//  Arkanoid
//
//  Created by Alekzander on 03/03/2019.
//  Copyright © 2019 Pakass. All rights reserved.
//

#include "EnemyFighter.hpp"

EnemyFighter::EnemyFighter(){
    enemyTexture.loadFromFile("../../Assets/Sprites/Enemy_Explode_Sequence/EnemyTurnLeft0000_00000.png");
    for (int i=0; i < ENEMY_NUM; i++){
        enemySprite[i].setTexture(enemyTexture);
        enemySprite[i].setScale(TEXTURE_SCALE / enemySprite[i].getLocalBounds().width, TEXTURE_SCALE / enemySprite[i].getLocalBounds().height);
        enemySprite[i].setPosition(DEFAULY_ENEMY_X, DEFAULY_ENEMY_Y);
    }
    srand(time(NULL));
}

bool EnemyFighter::isEnemyDrawable(short enemyNum){
    return isDrawable[enemyNum];
}

sf::Sprite EnemyFighter::getDrawEnemy(short enemyNum){
    return enemySprite[enemyNum];
}

short EnemyFighter::getEnemyNum(){
    return ENEMY_NUM;
}

void EnemyFighter::setDifficulty(short diff){
    if (diff >= ENEMY_NUM){
        enemyOnScreen = ENEMY_NUM;
    } else {
        enemyOnScreen = diff;
    }
}

int EnemyFighter::toInt(float inputFloat){
    return inputFloat;
}

void EnemyFighter::setScreenSize(float setX, float setY){
    screenSizeX = setX;
    screenSizeY = setY;
}

void EnemyFighter::setFrameRate(int setFrameRate){
    frameRate = setFrameRate;
}

float EnemyFighter::speedFromFrameRate(int frameRate, float speed){
    return frameRate * speed;
}

float EnemyFighter::randomEnemyPositionX(int iterator){
    float positionX = rand() % toInt(screenSizeX);
    if(positionX - enemySprite[iterator].getLocalBounds().width <= 0){
        positionX = positionX + enemySprite[iterator].getLocalBounds().width;
    }
    if(positionX + enemySprite[iterator].getLocalBounds().width >= screenSizeX){
        positionX = positionX - enemySprite[iterator].getLocalBounds().width;
    }
    return positionX;
}

float EnemyFighter::randomEnemyPositionY(int iterator){
    int shift = random()%3 + 1;
    return 0.0 - enemySprite[iterator].getLocalBounds().height * shift;
}

void EnemyFighter::animationStep(){
    for (int i=0; i < enemyOnScreen; i++){
        if (isDrawable[i]==false) {
            isDrawable[i]=true;
            enemySprite[i].setPosition(randomEnemyPositionX(i), randomEnemyPositionY(i));
        } else {
            if (enemySprite[i].getPosition().y > screenSizeY) {
                enemySprite[i].setPosition(randomEnemyPositionX(i), randomEnemyPositionY(i));
            } else {
                enemySprite[i].setPosition(enemySprite[i].getPosition().x, enemySprite[i].getPosition().y + speedFromFrameRate(frameRate, ENEMY_SPEED));
            }
        }
        //std::cout << "enemy n: " << i << " x: " << enemySprite[i].getPosition().x << " y: " << enemySprite[i].getPosition().y << std::endl;
        //std::cout << screenSizeX << " " << screenSizeY << std::endl;
    }
}

short EnemyFighter::isCollision(float playerCornerUpX, float playerCornerUpY, float xSize, float ySize, bool debug){
    for (int i=0; i<ENEMY_NUM; i++){
        if (isDrawable[i]==true) {
            if (debug == true){
                float playerCornerX = playerCornerUpX +xSize;
                float playerCornerY = playerCornerUpY + ySize;
                float enCornerUpX = enemySprite[i].getPosition().x;
                float enCornerUpY = enemySprite[i].getPosition().y;
                float enCornerX = enemySprite[i].getGlobalBounds().width;
                float enCornerY = enemySprite[i].getGlobalBounds().height;
                std::cout << "p up x: " << playerCornerUpX << " p up y: " << playerCornerUpY << " p down x: " << playerCornerX << " p down y: " << playerCornerY << std::endl;
                std::cout << "e up x: " << enCornerUpX << " e up y: " << enCornerUpY << " e down x: " << enCornerX << " e down y: " << enCornerY << std::endl;
                if ((playerCornerY >= enCornerUpY) && (playerCornerY <= enCornerUpY + enCornerY) && ((((enCornerUpX + enCornerX) >= playerCornerUpX) && (enCornerUpX + enCornerX) <= playerCornerX) || ((enCornerUpX >= playerCornerUpX) && (enCornerUpX <= playerCornerX)))) {
                 return i;
                }
            }
            else {
                if (((playerCornerUpY + ySize) >= enemySprite[i].getPosition().y) && ((playerCornerUpY + ySize) <= enemySprite[i].getPosition().y + enemySprite[i].getGlobalBounds().height) && ((((enemySprite[i].getPosition().x + enemySprite[i].getGlobalBounds().width) >= playerCornerUpX) && (enemySprite[i].getPosition().x + enemySprite[i].getGlobalBounds().width) <= (playerCornerUpX + xSize)) || ((enemySprite[i].getPosition().x >= playerCornerUpX) && (enemySprite[i].getPosition().x <= (playerCornerUpX + xSize))))) {
                    return i;
                }
            }
        }
    }
    return -1;
}

void EnemyFighter::resetEnemy(short enemyNum){
    isDrawable[enemyNum] = false;
}
