//
//  EnemyFighter.hpp
//  Arkanoid
//
//  Created by Alekzander on 03/03/2019.
//  Copyright © 2019 Pakass. All rights reserved.
//

#ifndef EnemyFighter_hpp
#define EnemyFighter_hpp

#include <SFML/Graphics.hpp>
#include <time.h>
#include <iostream>

#define ENEMY_NUM 10

#define TEXTURE_SCALE 150.0

#define DEFAULY_ENEMY_X 100.0
#define DEFAULY_ENEMY_Y 100.0

#define ENEMY_SPEED 0.1

class EnemyFighter {
private:
    sf::Texture enemyTexture;
    sf::Sprite enemySprite[ENEMY_NUM];
    short enemyOnScreen;
    int toInt(float);
    bool isDrawable[ENEMY_NUM] = { false };
    int frameRate;
    float screenSizeX;
    float screenSizeY;
    float speedFromFrameRate(int frameRate, float speed);
    float randomEnemyPositionX(int iterator);
    float randomEnemyPositionY(int iterator);
public:
    EnemyFighter();
    void setDifficulty(short diff);
    void setScreenSize(float setX, float setY);
    void setFrameRate(int setFrameRate);
    void animationStep();
    sf::Sprite getDrawEnemy(short enemyNum);
    bool isEnemyDrawable(short enemyNum);
    void resetEnemy(short enemyNum);
    short getEnemyNum();
    short isCollision(float playerCornerUpX, float playerCornerUpY, float xSize, float ySize, bool debug);
};

#endif /* EnemyFighter_hpp */
