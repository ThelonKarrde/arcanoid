//
//  GraphicWork.cpp
//  Arkanoid
//
//  Created by Alekzander on 16/02/2019.
//  Copyright © 2019 Pakass. All rights reserved.
//

#include "GraphicWork.hpp"

GraphicWork::GraphicWork(int input_width, int input_height, float input_speed, int framerate){
    fighters.setDifficulty(3);
    fighters.setScreenSize(input_width, input_height);
    fighters.setFrameRate(framerate);
    width = input_width;
    height = input_height;
    speed = input_speed;
    blastNum = 0;
    srand(time(NULL));
    starSize.x=2.0f;
    starSize.y=2.0f;
    window.create(sf::VideoMode(width, height), "Arcanoid");
    window.setFramerateLimit(framerate);
    framerate_internal = framerate;
    for (int i=0; i<STARS_NUM; i++){
        stars[i].setSize(starSize);
        stars[i].setPosition(rand() % width + 1, rand() % height + 1);
        stars[i].setFillColor(sf::Color::White);
    }
    animationCounter = 0;
}

void GraphicWork::playerSetup(float input_x, float input_y){
    player.SetPlayerParameters(input_x, input_y, width, height, framerate_internal, PLAYER_SHIP_INERSION);
}

void GraphicWork::starsMoving(){
    for (int i=0; i<STARS_NUM; i++){
        stars[i].setPosition(stars[i].getPosition().x, stars[i].getPosition().y + speed);
        if(stars[i].getPosition().y > height){
            stars[i].setPosition(rand() % width + 1, -3.0);
        }
    }
}

int GraphicWork::spriteInit(){
    if(!backgroundTexture.loadFromFile("../../Assets/Sprites/SpaceBG_Overlay.png")){
        return EXIT_FAILURE;
    }
    backgroundSprite.setTexture(backgroundTexture);
    backgroundSprite.setScale(window.getSize().x / backgroundSprite.getLocalBounds().width, window.getSize().y / backgroundSprite.getLocalBounds().height);
    player.InitGraphics();
    
    if(!playerLaserTexture.loadFromFile("../../Assets/Sprites/laser.png")){
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

void GraphicWork::setupBlastSprite(int blast_num){
    playerLaserSprite[blastNum].setTexture(playerLaserTexture);
    playerLaserSprite[blastNum].setScale(playerLaserSprite[blastNum].getLocalBounds().width / LASER_SCALE / playerLaserSprite[blastNum].getLocalBounds().width, playerLaserSprite[blastNum].getLocalBounds().height / LASER_SCALE / playerLaserSprite[blastNum].getLocalBounds().height);
}



void GraphicWork::playerBlast(){
    setupBlastSprite(blastNum);
    playerLaserSprite[blastNum].setPosition(player.GetPlayerPositionX() + player.GetPlayerLocalBoundsWidth() * player.GetPlayerScaleX() / 2 - playerLaserSprite[blastNum].getLocalBounds().width * playerLaserSprite[blastNum].getScale().x / 2, player.GetPlayerPositionY() + 25.0);
    blastNum++;
}

void GraphicWork::blastAnimation(){
    if(blastNum > 0){
        for (int i=0; i<blastNum; i++){
            if (playerLaserSprite[i].getPosition().y > 0 - playerLaserSprite[i].getLocalBounds().height) {
                playerLaserSprite[i].setPosition(playerLaserSprite[i].getPosition().x, playerLaserSprite[i].getPosition().y - speedFromFrameRate(framerate_internal, BLAST_SPEED));
            }
            if (playerLaserSprite[i].getPosition().y < 0 -  playerLaserSprite[i].getLocalBounds().height) {
                for (int j=0; j<blastNum; j++) {
                    playerLaserSprite[j] = playerLaserSprite[j+1];
                }
                blastNum--;
            }
        }
    }
}

float GraphicWork::speedFromFrameRate(int frameRate, float speed){
    return frameRate * speed;
}

int GraphicWork::renderLoop(){
    while (window.isOpen()) {
        sf::Event outEvent;
        while(window.pollEvent(outEvent)){
            if(outEvent.type == sf::Event::Closed){
                window.close();
            }
            if(outEvent.type == sf::Event::EventType::KeyPressed){
                if(outEvent.key.code == sf::Keyboard::Left){
                    player.SetPlayerSpeedXLeft(PLAYER_SPEED);
                }
                if(outEvent.key.code == sf::Keyboard::Right){
                    player.SetPlayerSpeedXRight(PLAYER_SPEED);
                }
                if(outEvent.key.code == sf::Keyboard::Up){
                    player.SetPlayerSpeedYUp(PLAYER_SPEED);
                }
                if(outEvent.key.code == sf::Keyboard::Down){
                    player.SetPlayerSpeedYDown(PLAYER_SPEED);
                }
                if(outEvent.key.code == sf::Keyboard::Space){
                    playerBlast();
                }
                if(outEvent.key.code == sf::Keyboard::Q){
                    window.close();
                }
            }
        }
        
        for(int i=0; i<blastNum; i++){
            short blastCollision = fighters.isCollision(playerLaserSprite[i].getPosition().x, playerLaserSprite[i].getPosition().y, playerLaserSprite[i].getLocalBounds().width, playerLaserSprite[i].getLocalBounds().height, false);
            std::cout << "bladcollision: " << blastCollision << std::endl;
            if(blastCollision != -1){
                blastNum--;
                fighters.resetEnemy(blastCollision);
            }
        }
        short collisionEnemyWithPlayer = fighters.isCollision(player.GetPlayerPositionX(), player.GetPlayerPositionY(), player.GetPlayerGlobalBoundsWidth(), player.GetPlayerGlobalBoundsHeight(), false);
        if(collisionEnemyWithPlayer != -1){
            fighters.resetEnemy(collisionEnemyWithPlayer);
        }
        blastAnimation();
        fighters.animationStep();
        player.ShipAnimation();
        player.MovePlayerShip();
        window.clear();
        window.draw(backgroundSprite);
        for(int i=0; i < STARS_NUM; i++) {
            window.draw(stars[i]);
        }
        if (blastNum > 0) {
            for (int i=0; i < blastNum; i++){
                window.draw(playerLaserSprite[i]);
            }
        }
        for (int i=0; i < fighters.getEnemyNum(); i++){
            if(fighters.isEnemyDrawable(i) == true){
                window.draw(fighters.getDrawEnemy(i));
            }
        }
        window.draw(player.GetPlayerSprite());
        window.display();
        starsMoving();
    }
    return EXIT_SUCCESS;
}
