//
//  main.cpp
//  Arkanoid
//
//  Created by Alekzander on 15/02/2019.
//  Copyright © 2019 Pakass. All rights reserved.
//
#include "GraphicWork.hpp"

#define WIDTH 1280
#define HEIGHT 720

int main(int argc, const char * argv[]) {
    GraphicWork render(WIDTH, HEIGHT, 1.0, 60);
    render.playerSetup(130.0, 130.0);
    render.spriteInit();
    render.renderLoop();
    return EXIT_SUCCESS;
}
