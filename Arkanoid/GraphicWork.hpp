//
//  GraphicWork.hpp
//  Arkanoid
//
//  Created by Alekzander on 16/02/2019.
//  Copyright © 2019 Pakass. All rights reserved.
//

#ifndef GraphicWork_hpp
#define GraphicWork_hpp

#include <stdio.h>
#include <SFML/Graphics.hpp>
#include <time.h>
#include <iostream>
#include "EnemyFighter.hpp"
#include "PlayerFighter.hpp"
#include <unistd.h>

#define STARS_NUM 100

#define LASER_NUMS 1000

#define PLAYER_ANIMATION_FRAMES 9


#define MOVE_UP 1
#define MOVE_DOWN 2
#define MOVE_RIGHT 3
#define MOVE_LEFT 4

#define PLAYER_SPEED 0.8
#define PLAYER_SHIP_INERSION 0.125

#define BLAST_SPEED 1.0

#define LASER_SCALE 6

class GraphicWork {
private:
    int width;
    int height;
    int animationCounter;
    float speed;
    float inersion;
    int framerate_internal;
    int blastNum;
    EnemyFighter fighters;
    PlayerFighter player;
    sf::RenderWindow window;
    sf::Sprite backgroundSprite;
    sf::Sprite playerLaserSprite[LASER_NUMS];
    sf::Texture playerLaserTexture;
    sf::Texture backgroundTexture;
    sf::RectangleShape stars[STARS_NUM];
    sf::Vector2f starsCoord[STARS_NUM];
    sf::Vector2f starSize;
    void starsMoving();
    void playerBlast();
    void setupBlastSprite(int);
    void blastAnimation();
    float speedFromFrameRate(int frameRate, float speed);
public:
    GraphicWork(int, int, float, int);
    void playerSetup(float, float);
    int spriteInit();
    int renderLoop();
};

#endif /* GraphicWork_hpp */
