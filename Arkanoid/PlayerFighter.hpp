//
//  PlayerFighter.hpp
//  Arkanoid
//
//  Created by Alekzander on 09/03/2019.
//  Copyright © 2019 Pakass. All rights reserved.
//

#ifndef PlayerFighter_hpp
#define PlayerFighter_hpp

#include <stdio.h>
#include <SFML/Graphics.hpp>
#include <iostream>

#define FIGHTER_ANIMATION_FRAMES 9

class PlayerFighter{
private:
    sf::Texture fighterTexture;
    sf::Texture fighterAnimationLeftTexture[FIGHTER_ANIMATION_FRAMES];
    sf::Texture fighterAnimationRightTexture[FIGHTER_ANIMATION_FRAMES];
    sf::Sprite fighterSprite;
    sf::Sprite fighterAnimationLeftSprite;
    sf::Sprite fighterAnimationRightSprite;
    void SetupAnimationSpriteLeft(short frame);
    void SetupAnimationSpriteRight(short frame);
    float SpeedFromFrameRate(float input_frame_rate, float input_speed);
    short animationFramesNumber;
    short framteRate;
    short animationCounter;
    float playerSizeX;
    float playerSizeY;
    float playerSpeedXRight;
    float playerSpeedXLeft;
    float playerSpeedYUp;
    float playerSpeedYDown;
    float playerPositionX;
    float playerPositionY;
    float playerShipIntertion;
    float screen_width;
    float screen_height;
    bool isPlayerShipAnimationLeft;
    bool isPlayerShipAnimationRight;
public:
    PlayerFighter();
    void SetPlayerParameters(float input_x, float input_y, float input_width, float input_height, short input_framte_rate, float input_inertion);
    int InitGraphics();
    float GetPlayerPositionX();
    float GetPlayerPositionY();
    float GetPlayerGlobalBoundsWidth();
    float GetPlayerGlobalBoundsHeight();
    float GetPlayerLocalBoundsWidth();
    float GetPlayerLocalBoundsHeight();
    float GetPlayerScaleX();
    float GetPlayerScaleY();
    void MovePlayerShip();
    void ShipAnimation();
    void SetPlayerSpeedXRight(float input_speed);
    void SetPlayerSpeedXLeft(float input_speed);
    void SetPlayerSpeedYUp(float input_speed);
    void SetPlayerSpeedYDown(float input_speed);
    sf::Sprite GetPlayerSprite();
};

#endif /* PlayerFighter_hpp */
