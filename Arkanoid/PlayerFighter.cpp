//
//  PlayerFighter.cpp
//  Arkanoid
//
//  Created by Alekzander on 09/03/2019.
//  Copyright © 2019 Pakass. All rights reserved.
//

#include "PlayerFighter.hpp"

PlayerFighter::PlayerFighter(){
    animationCounter = 0;
}

int PlayerFighter::InitGraphics(){
    if(!fighterTexture.loadFromFile("../../Assets/Sprites/Player_Turn_Right/Right0000.png")){
        return EXIT_FAILURE;
    }
    for (int i = 0; i < FIGHTER_ANIMATION_FRAMES; i++){
        std::string filename_left="../../Assets/Sprites/Player_Turn_Left/Left000";
        std::string filename_right="../../Assets/Sprites/Player_Turn_Right/Right000";
        filename_left.append(std::to_string(i));
        filename_left.append(".png");
        filename_right.append(std::to_string(i));
        filename_right.append(".png");
        if(!fighterAnimationLeftTexture[i].loadFromFile(filename_left)){
            return EXIT_FAILURE;
        };
        if(!fighterAnimationRightTexture[i].loadFromFile(filename_right)){
            return EXIT_FAILURE;
        };
    }
    fighterSprite.setTexture(fighterTexture);
    fighterSprite.setScale(playerSizeX / fighterSprite.getLocalBounds().width,  playerSizeY / fighterSprite.getLocalBounds().height);
    fighterSprite.setPosition(playerPositionX, playerPositionY);
    SetupAnimationSpriteLeft(0);
    SetupAnimationSpriteRight(0);
    return EXIT_SUCCESS;
}

void PlayerFighter::SetPlayerParameters(float input_x, float input_y, float input_width, float input_height, short input_frame_rate, float input_inertion){
    playerShipIntertion = input_inertion;
    framteRate = input_frame_rate;
    screen_width = input_width;
    screen_height = input_height;
    playerSizeX = input_x;
    playerSizeY = input_y;
    playerSpeedXRight = 0.0;
    playerSpeedXLeft = 0.0;
    playerSpeedYUp = 0.0;
    playerSpeedYDown = 0.0;
    playerPositionX = screen_width / 2.0 - playerSizeX / 2.0;
    playerPositionY = screen_height - playerSizeY;
    isPlayerShipAnimationLeft = false;
    isPlayerShipAnimationRight = false;
}

void PlayerFighter::SetupAnimationSpriteLeft(short frame){
    fighterAnimationLeftSprite.setTexture(fighterAnimationLeftTexture[frame]);
    fighterAnimationLeftSprite.setScale(playerSizeX / fighterAnimationLeftSprite.getLocalBounds().width, playerSizeY / fighterAnimationLeftSprite.getLocalBounds().height);
}

void PlayerFighter::SetupAnimationSpriteRight(short frame){
    fighterAnimationRightSprite.setTexture(fighterAnimationRightTexture[frame]);
    fighterAnimationRightSprite.setScale(playerSizeX / fighterAnimationRightSprite.getLocalBounds().width, playerSizeY / fighterAnimationRightSprite.getLocalBounds().height);
}

float PlayerFighter::GetPlayerPositionX(){
    return fighterSprite.getPosition().x;
}

float PlayerFighter::GetPlayerPositionY(){
    return fighterSprite.getPosition().y;
}

float PlayerFighter::GetPlayerLocalBoundsWidth(){
    return fighterSprite.getLocalBounds().width;
}

float PlayerFighter::GetPlayerLocalBoundsHeight(){
    return fighterSprite.getLocalBounds().height;
}

float PlayerFighter::GetPlayerGlobalBoundsWidth(){
    return fighterSprite.getGlobalBounds().width;
}

float PlayerFighter::GetPlayerGlobalBoundsHeight(){
    return fighterSprite.getGlobalBounds().height;
}

float PlayerFighter::GetPlayerScaleX(){
    return fighterSprite.getScale().x;
}

float PlayerFighter::GetPlayerScaleY(){
    return fighterSprite.getScale().y;
}

void PlayerFighter::MovePlayerShip(){
    if(fighterSprite.getPosition().y <= 1.0 && playerSpeedYUp > 0){
        playerSpeedYUp = 0.0;
    }
    if(fighterSprite.getPosition().y >= screen_height - playerSizeY && playerSpeedYDown > 0){
        playerSpeedYDown = 0.0;
    }
    if(fighterSprite.getPosition().x >= screen_width - playerSizeX && playerSpeedXRight > 0){
        playerSpeedXRight = 0.0;
    }
    if(fighterSprite.getPosition().x <= 0.0 && playerSpeedXLeft > 0){
        playerSpeedXLeft = 0.0;
    }
    if (fighterSprite.getPosition().y > 1.0 && playerSpeedYUp > 0){
        fighterSprite.setPosition(fighterSprite.getPosition().x, fighterSprite.getPosition().y - playerSpeedYUp);
        fighterAnimationRightSprite.setPosition(fighterSprite.getPosition().x, fighterSprite.getPosition().y);
        fighterAnimationLeftSprite.setPosition(fighterSprite.getPosition().x, fighterSprite.getPosition().y);
        playerSpeedYUp = playerSpeedYUp - SpeedFromFrameRate(framteRate, playerShipIntertion);
    }
    if (fighterSprite.getPosition().y < screen_height - playerSizeY && playerSpeedYDown > 0){
        fighterSprite.setPosition(fighterSprite.getPosition().x, fighterSprite.getPosition().y + playerSpeedYDown);
        fighterAnimationRightSprite.setPosition(fighterSprite.getPosition().x, fighterSprite.getPosition().y);
        fighterAnimationLeftSprite.setPosition(fighterSprite.getPosition().x, fighterSprite.getPosition().y);
        playerSpeedYDown = playerSpeedYDown - SpeedFromFrameRate(framteRate, playerShipIntertion);
    }
    if(fighterSprite.getPosition().x < screen_width - playerSizeX && playerSpeedXRight > 0){
        fighterSprite.setPosition(fighterSprite.getPosition().x + playerSpeedXRight, fighterSprite.getPosition().y);
        fighterAnimationRightSprite.setPosition(fighterSprite.getPosition().x, fighterSprite.getPosition().y);
        fighterAnimationLeftSprite.setPosition(fighterSprite.getPosition().x, fighterSprite.getPosition().y);
        playerSpeedXRight = playerSpeedXRight - SpeedFromFrameRate(framteRate, playerShipIntertion);
    }
    if(fighterSprite.getPosition().x > 0.0 && playerSpeedXLeft > 0){
        fighterSprite.setPosition(fighterSprite.getPosition().x - playerSpeedXLeft, fighterSprite.getPosition().y);
        fighterAnimationRightSprite.setPosition(fighterSprite.getPosition().x, fighterSprite.getPosition().y);
        fighterAnimationLeftSprite.setPosition(fighterSprite.getPosition().x, fighterSprite.getPosition().y);
        playerSpeedXLeft = playerSpeedXLeft - SpeedFromFrameRate(framteRate, playerShipIntertion);
    }
}

float PlayerFighter::SpeedFromFrameRate(float input_frame_rate, float input_speed){
    return input_frame_rate * input_speed;
}

void PlayerFighter::ShipAnimation(){
    if(playerSpeedXLeft <= 0.0 && animationCounter == 0){
        isPlayerShipAnimationLeft = false;
        return;
    }
    if (playerSpeedXRight <= 0.0 && animationCounter == 0) {
        isPlayerShipAnimationRight = false;
        return;
    }
    if(isPlayerShipAnimationLeft == true){
        if (playerSpeedXLeft <= 0.0 && animationCounter > 0) {
            SetupAnimationSpriteLeft(animationCounter);
            if(animationCounter - 1 >= 0){
                animationCounter--;
                return;
            } else {
                return;
            }
        }
        if (playerSpeedXLeft != 0.0 && animationCounter <= FIGHTER_ANIMATION_FRAMES) {
            SetupAnimationSpriteLeft(animationCounter);
            if (animationCounter + 1 <= FIGHTER_ANIMATION_FRAMES) {
                animationCounter++;
                return;
            } else {
                return;
            }
        }
    }
    if(isPlayerShipAnimationRight == true){
        if (playerSpeedXRight <= 0.0 && animationCounter > 0) {
            SetupAnimationSpriteRight(animationCounter);
            if(animationCounter - 1 >= 0){
                animationCounter--;
                return;
            } else {
                return;
            }
        }
        if (playerSpeedXRight != 0.0 && animationCounter <= FIGHTER_ANIMATION_FRAMES) {
            SetupAnimationSpriteRight(animationCounter);
            if (animationCounter + 1 <= FIGHTER_ANIMATION_FRAMES) {
                animationCounter++;
                return;
            } else {
                return;
            }
        }
    }
}

sf::Sprite PlayerFighter::GetPlayerSprite(){
    if(isPlayerShipAnimationLeft == true || isPlayerShipAnimationRight == true){
        if(isPlayerShipAnimationRight == true) {
            return fighterAnimationRightSprite;
        }
        if(isPlayerShipAnimationLeft == true){
            return fighterAnimationRightSprite;
        }
    }
    return fighterSprite;
}

void PlayerFighter::SetPlayerSpeedXRight(float input_speed){
    playerSpeedXRight = SpeedFromFrameRate(framteRate, input_speed);
    isPlayerShipAnimationRight = true;
}

void PlayerFighter::SetPlayerSpeedXLeft(float input_speed){
    playerSpeedXLeft = SpeedFromFrameRate(framteRate, input_speed);
    isPlayerShipAnimationLeft = true;
}

void PlayerFighter::SetPlayerSpeedYUp(float input_speed){
    playerSpeedYUp = SpeedFromFrameRate(framteRate, input_speed);
}

void PlayerFighter::SetPlayerSpeedYDown(float input_speed){
    playerSpeedYDown = SpeedFromFrameRate(framteRate, input_speed);
}
